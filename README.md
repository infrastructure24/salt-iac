# Salt-iac

- This project is an attempt to revive a Kubernetes cluster that's been down for almost a year.
- If this project is successful the cluster will be back online and managed entirely through this repo.
- Websites are then deployed to this cluster through [the greenhouseaffectors group](https://gitlab.com/greenhouseaffectors)

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Getting started
These states are applied to a kubic cluster. For docs on setting up the kubic cluster see [this repo](https://gitlab.com/infrastructure24/infrastructure).

To apply the states in this repo add it as a __git fileserver backend__ as described in the [states README](./stsates/README.md). Then add the pillars described in the [pillars README](./pillars/README.md) so these states have the variables they need.

To confirm that these states were successful, use the [gitlab-ci.yml](./gitlab-ci.yml) to run some salt tests with the salt api. For more on this see the salt-api...

### Install Pre-reqs

```
transactional-update pkg install python-pygit2 kubernetes-salt salt-api salt-bash-completion salt-doc salt-transactional-update
```

- Description of dependencies:
  - The `pygit2` git filesystem (`GitFS`) provider enables the salt master to run git commands.
  - `salt-api` is used by the [salt-api-docs.md](./salt-api-docs.md) to run salt commands without SSHing into master.
  - `kubernetes-salt salt-bash-completion salt-doc salt-transactional-update` aren't strictly necessary for this repo (yet), but can be helpful.

```
transactional-update pkg update salt
```

- making sure we have the most recent salt version

- Reboot the master so it boots into the new snapshot.

## Configure `GitFS`

- `GitFS` [GitFS docs](./states/README.md)

## Pillars

Pillars store data on the master... see [these docs](./pillars/README.md)

## Usage

Changes to salt states will get pulled to the master by the gitfs backend. You can apply these manually from the container created with docker-compose with `pepper '*' state.apply`

## Salt-api

This repo uses salt-api from the `.gitlab-ci.yml` to ensure that these configurations were applied successfully. See the [salt api docs](./salt-api-docs.md) for how to set this up.

## Support

Open issues on this repo if you need support. This project is a hobby project so there are no support guarantees.

## Roadmap

- allow commands other than just test
- disk management
- [DDNS](https://docs.saltproject.io/en/latest/ref/states/all/salt.states.ddns.html)
- General testing like ping
- cockpit
- kubernetes
  - add hostname to certs...
- k8s stuff to install from here
  - rook
  - argo-cd
  - traefik (maybe use gitlab's nginx???)
- [salt gui](https://github.com/erwindon/SaltGUI)
- [snapper](https://docs.saltproject.io/en/latest/ref/states/all/salt.states.snapper.html)

## Contributing
Contrubuting workflow:
- Open an issue
- Checkout a branch related to the issue
- Submit a merge request from the branch to master make sure the test cases pass

## Authors and acknowledgment
- Me
- The Greenhouse affectors

## License
MIT

## Project status
- Start of project 11/20/2021


