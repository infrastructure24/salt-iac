## Pillars

- `pillar_roots` in `/etc/salt/master` sets `/srv/pillar` as the default `base` pillar folder.
- Create `/srv/pillar/top.sls` with the following contents

```yaml
base:
  '*':
    - data
```

- Then create `/srv/pillar/data.sls` with the following contents

```yaml
info: some data
```

- Run `salt '*' saltutil.refresh_pillar` to ensure the pillar cache is populated.
- Verify the pillar is populated:

```
master:~ # salt '*' pillar.items
thor.verizon.net:
    ----------   
    info:        
        some data
hpe.verizon.net:
    ----------
    info:
        some data
```

### Using Pillars from States

- 

### Pillars Docs

- [reference](https://docs.saltproject.io/en/latest/ref/configuration/master.html#pillar-configuration-master)
- [walkthrough](https://docs.saltproject.io/en/latest/topics/tutorials/pillar.html)