# https://docs.saltproject.io/en/latest/ref/states/all/salt.states.pkg.html
# https://docs.saltproject.io/en/3004/ref/modules/all/salt.modules.pkg.html
# https://docs.saltproject.io/en/3004/ref/executors/all/salt.executors.transactional_update.html

# note the transactional_update executor is installed by default. (see the following)
# cat /etc/salt/minion.d/transactional_update.conf
# Enable the transactional_update executor
# module_executors:
#   - transactional_update
#   - direct_call

# You need to add the following
# Replace the list of default modules that all the functions
# are delegated to `transactional_update.call()`
# delegated_modules: [cmd, pkg]

# Replace the list of default functions that are delegated to
# `transactional_update.call()`
# delegated_functions: [pip.install]


# using the module state to run transactional update commands from this state
# https://docs.saltproject.io/en/latest/ref/states/all/salt.states.module.html
# https://docs.saltproject.io/en/master/ref/modules/all/salt.modules.transactional_update.html#salt.modules.transactional_update.apply_

# salt '*' transactional_update.patch snapshot="continue"
# "/etc/salt/minion.d/transactional update.conf"
# 
# Had to install salt-transactional-update on thor
#
# need to add the following to "/etc/salt/minion.d/module.conf" on the minions and restart them
# use_superseded:
#   - module.run
# salt '*'
# test.random_hash:
#   module.run:
#     - test.random_hash:
#       - size: 42
#       - hash_type: sha256

# maybe use cmd module https://ansible-cn.readthedocs.io/en/latest/ref/states/all/salt.states.cmd.html
env: 
# salt.exceptions.CommandExecutionError: Cannot call transactional-update from within transactional-update environment!
  module.run:
    - transactional_update.patch:
      - snapshot: continue
    # - transactional_update.call: 
    #   - test.ping
      # - pkg_install:
        # - cockpit
      # cockpit-podman cockpit-system microos_cockpit patterns-microos-cockpit 
      # one long pkg command???
      # - snapshot: continue

anotherEnv:
  module.run:
    - transactional_update.call: 
      - test.ping

yetAnotherEnv:
  module.run:
    - transactional_update.pkg_install: 
      - pkg: cockpit
  
  #  cockpit
  # cockpit-podman
  # cockpit-system
  # microos_cockpit
  # patterns-microos-cockpit
