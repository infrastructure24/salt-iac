#! /bin/bash
# takes URL, UN, and PW
# updates noip's records as described here https://www.noip.com/integrate/request
# salt cron job: https://docs.saltproject.io/en/latest/ref/states/all/salt.states.cron.html
# maybe do this in k8s??
#MYIP=${curl ifconfig.me} ( this is optional)

# example of request string

# http://username:password@dynupdate.no-ip.com/nic/update?hostname=mytest.example.com&myip=192.0.2.25


# raw header

# GET /nic/update?hostname=mytest.example.com&myip=192.0.2.25 HTTP/1.1
# Host: dynupdate.no-ip.com
# Authorization: Basic base64-encoded-auth-string
# User-Agent: Company DeviceName-Model/FirmwareVersionNumber maintainer-contact@example.com

# they user User-Agent to tell what's making the request


# Docker container with no-ip DDNS support: https://hub.docker.com/r/qmcgaw/ddns-updater