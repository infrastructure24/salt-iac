# https://github.com/saltstack-formulas/ddns-client-formula
# https://www.noip.com/integrate
# https://www.noip.com/integrate/request
# get your IP with: curl `ifconfig.me`
  # https://www.tutorialspoint.com/find-my-public-ip-address-from-linux-command-line
ddns:
  test.nop:
    - name: {{ pillar['info'] }}
    - something: else
    - foo: bar
  # if using a TSIG key need dnspython installed (not sure)
  # https://docs.saltproject.io/en/latest/ref/states/all/salt.states.ddns.html
  # ddns.present:
  #   - zone: *.greenhouseaffectors.com
  #   - ttl: 60
  #   - data: 1.1.1.1
  #   - nameserver: dynupdate.no-ip.com
  # ddns.present:
  #   - zone: *.sellmyshift.com
  #   - ttl: 60
  #   - data: <data ip>
  #   - nameserver: <nameserver ip>
   # mynutrients123