# all pkgs will be installed here
# !!!apply with salt '*' transactional-update.apply "mypkgs" activate_transaction=True
# ^check that and add it to .gitlab-ci.yml^
# https://docs.saltproject.io/en/latest/ref/states/all/salt.states.pkg.html
mypkgs:
  pkg.installed:
    - pkgs:
      - vim
      - salt-bash-completion
      - salt-doc
      - kubernetes-salt

      # maybe patterns are installed differently
      # zypper info -t pattern microos_cockpit
      - cockpit-packagekit
      - cockpit-podman
      - cockpit-system
      # - patterns-microos-cockpit
      # more cockpit stuff:
      - cockpit
      - cockpit-storaged 
      - cockpit-tests 
      - cockpit-podman
      - cockpit-networkmanager
      - cockpit-system 
      - cockpit-ws
      # # zypper info -t pattern kubeadm
      - autofs
      - busybox-k8s-yaml
      - ceph-common
      - cri-o                           
      - cri-o-kubeadm-criconfig         
      - cri-tools                       
      # - docker-kubic                    
      # - docker-kubic-kubeadm-criconfig  
      - flannel-k8s-yaml                
      - health-checker-plugins-kubic    
      - hello-kubic-k8s-yaml            
      - helm                            
      - kube-prometheus-k8s-yaml        
      - kuberlr                         
      - kubernetes-client               
      - kubernetes-kubeadm              
      - kubernetes-kubelet              
      - kured-k8s-yaml                  
      - lvm2                            
      - metallb-k8s-yaml                
      - nfs-client                      
      - nfs-client-provisioner-k8s-yaml 
      # - patterns-base-basesystem        
      # - patterns-containers-kubeadm     
      # - patterns-microos-basesystem     
      - rook-k8s-yaml                   
      - rpcbind                         
      - weave-k8s-yaml
      # more kubernetes stuff
      # - patterns-containers-kubeadm
      - python36-kubernetes
      - python39-kubernetes
      # end     


      # - baz