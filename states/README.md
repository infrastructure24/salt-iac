# Salt-state docs

Salt states are salt's declarative approach to configuration as code. Here is [salt's quickstart on creating states](https://docs.saltproject.io/en/latest/topics/tutorials/states_pt1.html) refer to it if you're new to states. To manually apply states use `salt '*' state.apply` or `salt '*' transactional_update.apply activate_transaction=True` to apply them in a new snapshot (more on transactional_update later).

The states in this folder are pulled from this gitlab repo to your salt cluster using a GitFS (git fileserver) backend described in the next section.

<details>
  <summary>GitFS section  (click to expand)</summary>

## Configure `GitFS`

Adding this repo as a [__Git Fileserver Backend__](https://docs.saltproject.io/en/latest/topics/tutorials/gitfs.html) pulls states from this repo onto the master as though you had put them in `/srv/salt` when using the `roots` fileserver backend.

### Add your SSH key to gitlab

- Add the SSH key from `~/.ssh/id_rsa.pub` to gitlab. Search online for how to do this [this may help](https://docs.gitlab.com/ee/ssh/).

### Add the `GitFS` Backend to `/etc/salt/master`

Carefully merging the following configurations into `/etc/salt/master` will add `git@gitlab.com:infrastructure24/salt-iac.git` as a `GitFS` backend so you can apply the salt states in this repo on your cluster. Don't simply copy and paste this; for each setting search the current setting in `/etc/salt/master` and edit it to look like the settings shown here.

```yaml
fileserver_backend:
  - git
  - roots

gitfs_remotes:
  - git@gitlab.com:infrastructure24/salt-iac.git: # note the trailing :
    # Sets which gitlab directory is synced to the salt master (same as gitfs_root)
    - root: states/ 
    # Gitlab uses main instead of master (same as gitfs_base)
    - base: main

# gitfs_pubkey and gitfs_privkey requires absolute paths
# These directories may be different for you.
gitfs_pubkey: '/root/.ssh/id_rsa.pub'
gitfs_privkey: '/root/.ssh/id_rsa'
```

### Verify the `GitFS` Configuration

- Ensure you can see the files in your repo with the following commands.
  - `salt-run fileserver.file_list backend=git`

- You may need to clear the fileserver cache and update the git fileserver.
  - `salt-run fileserver.clear_cache`
  - `salt-run fileserver.update`
    - `salt '*' saltutil.sync_states saltenv=git` might work??? https://docs.saltproject.io/en/latest/ref/modules/all/salt.modules.saltutil.html

- For more fileserver commands [see the saltstack docs](https://docs.saltproject.io/en/latest/ref/runners/all/salt.runners.fileserver.html). [also see these suse docs](https://documentation.suse.com/external-tree/en-us/suma/4.1/suse-manager/salt/salt-gitfs.html)

</details>


<details>
  <summary>Transactional-update section (click to expand)</summary>

## Transactional Updates

Some of Open Suse's distributions like microOS and kubic use [transactional updates](https://kubic.opensuse.org/documentation/man-pages/transactional-update.8.html) to make updates safer. Transactional updates work by creating each update in a new snapshot (called a transaction) so the system can be rolled back if needed. Once updates are made to the new transaction they are marked read-only to ensure they aren't changed accidentally (more on this [here](https://events19.linuxfoundation.org/wp-content/uploads/2017/11/Transactional-Updates-with-Btrfs-and-RPM-Ignaz-Forster-SUSE.pdf)). Because these files are marked read-only (except when creating a transaction), anything that changes them like installing new packages etc must be done within a new transaction. See the next sections for how to use transactional updates in salt.

### Pre-reqs

Install the `salt-transactional-update` package on *every* machine (not just the master) in your salt cluster. This may not be necessary after salt 3004 but for now it is.

```sh
transactional-update pkg install salt-transactional-update
```

__Make sure to reboot after calling this so the transaction gets applied__

### Using Transactional Update

You can use transactional updates through the module with commands like `salt '*' transactional_update.call test.ping` which will call `salt '*' test.ping` inside a new transaction. Note, for these changes to be applied you would have to reboot. You can apply states in a new transaction with `salt '*' transactional_update.apply activate_transaction=True`. I believe `activate_transaction` will reboot your machine

</details>