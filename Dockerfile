FROM ubuntu:latest
# TODO: use alpine
WORKDIR /
COPY ./entrypoint.sh /entrypoint.sh 
# you can also chown in the copy command
ENV DEBIAN_FRONTEND=noninteractive 
RUN ["chmod", "+x", "/entrypoint.sh"]
RUN apt -y update && apt install -y python3-pip vim curl git inetutils-ping ssh dnsutils
# TODO: don't install all this stuff when running from gitlab
RUN pip3 install salt-pepper ws4py
# adding an entrypoint script to avoid the 
ENTRYPOINT [ "/entrypoint.sh" ]

# delete old docker images with:
# build this with:
# docker-copose up --build