# Installing `salt-api` on the Salt Master

Applying these configurations will allow you to run salt commands from an externally (for example, from a gitlab runner).

### Run salt as `root`

- It's important to run salt as root so salt has access to various files including:
  - [__PAM__ files](https://github.com/saltstack/salt/issues/7762)
  - [salt settings files](https://docs.saltproject.io/en/latest/ref/configuration/nonroot.html)

- To make salt run as root, comment out `user:salt` or set `user:root`.
  - I also commented out `syndic_user: salt` which may or may not be necessary

### Install `salt-api` on the Salt Master

```
transactional-update pkg install kubernetes-salt salt-api salt-bash-completion salt-doc salt-transactional-update
```

### Create Keys

- run the following at the command line

```sh
master:~ # salt-call --local tls.create_self_signed_cert
local:
    Created Private Key: "/etc/pki/tls/certs/localhost.key." Created Certificate: "/etc/pki/tls/certs/localhost.crt."
```

### Configure External Authentication to the `salt-api`

- First, create a group on the master for receiving salt-api connections. Then add a user to that group.

```
# groupadd iac
# useradd --groups iac --create-home --shell /bin/bash gitlabsalt
```

- Then enable external authentication from group members by adding/uncommenting the following lines in `/etc/salt/master`.

```yaml
# The external auth system uses the Salt auth modules to authenticate and
# validate users to access areas of the Salt system.
# https://docs.saltproject.io/en/latest/topics/eauth/index.html#acl-eauth
external_auth:
  pam:
    iac%: # iac% means the iac group
      - '*': # allow for all minions
        - 'test.*' # allow test commands
```

- Note, these changes won't take effect until you restart the salt master in a later step.

### Configure the `salt-api`

- Add the following lines to `/etc/salt/master` telling it how to run the `salt-api`

```yaml
rest_cherrypy:
  port: 8000
  host: 0.0.0.0
  ssl_crt: /etc/pki/tls/certs/localhost.crt
  ssl_key: /etc/pki/tls/certs/localhost.key
  #disable_ssl: true
  #debug: True
```

### Restart the `salt-master` and Start `salt-api`

```
# pkill salt-master && pkill salt-api 
# salt-master -d && salt-api -d
```

- Alternatively use systemctl (maybe)

### Verify it works

To verify the salt api works, use pepper from the container created by the docker-compose script in this repo or the following curl command.

```sh
curl -k -vvv https://URL:8000/login -H Accept:application/json -H Content-Type:application/json -H X-Requested-With:XMLHttpRequest -d '{"username": "gitlabsalt", "password": "PASSWORD for gitlabsalt", "eauth": "pam"}'
```

For the docker-compose to work, first run `mv example.env .env`, edit it with your `salt-api` server's configuration, then run `docker-compose run pepper` and use commands like `pepper '*' test.ping` to run salt commands. (You may need to use `docker-compose build` first to build the container.)

## Salt-API References

- [`/etc/salt/master` salt-api configurations (rest_cherrypy)](https://docs.saltproject.io/en/latest/ref/netapi/all/salt.netapi.rest_cherrypy.html#module-salt.netapi.rest_cherrypy)
  - [salt-api cli options](https://docs.saltproject.io/en/latest/ref/cli/salt-api.html)
- [pepper](https://github.com/saltstack/pepper0)
- [salt states](https://docs.saltproject.io/en/latest/topics/tutorials/states_pt1.html)
- [external authentication](https://docs.saltproject.io/en/latest/topics/eauth/index.html)